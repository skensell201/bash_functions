# Useful bash functions

Полезные и не очень, bash функции.

* Функции - набор команд, которые можно вызывать много раз. Назначение функции - помочь вам сделать ваши сценарии bash более удобочитаемыми и избежать повторного написания одного и того же кода.
* Функции можно подключать в ~/.bashrc,~/.zshrc и т.п. и использовать как обычный вызов программ из терминала. (Примеры ниже)

## Как написать функцию
### Общее представление
```
$ cat new_bash_function

function_name () { <- Имя функции
  command1         <- Команды которые будут выполняться
  command2
}
```
## Полезный пример
### Функция которая:
- добавляет все файлы в индекс
- записывает изменения в репозитории (коммит) за коментарий берется имя текущей ветки и аргумент который введет пользователь
- отправляет изменения в репозиторий с текущей веткой
### Содержимое функции
```
$ cat gitpush

gitpush() {
   git add .
   git commit -m "$(git branch --show-current), $1"
   git push origin $(git branch --show-current)
}
```
### Текущая ветка DEVOPS-0001-test-project
 ```
$ pwd
 /home/user/TEST_PROJECT/(DEVOPS-0001-test-project)
```
### Запуск функции
```
$ gitpush 'ADD a new file for test'
```
### Посмотреть коммиты
```
$ git log

commit 5e0f2732b9097d55476409cb7506208aef1c1b76 (HEAD -> DEVOPS-0001-test-project, origin/DEVOPS-0001-test-project)
Author: Skensell <skense1@yandex.ru>
Date:   Thu Jan 28 16:56:15 2021 +0300

    DEVOPS-0001-test-project, ADD a new file for test
```

## Как добавить функцию в ~/.zshrc
### Создать каталог для функций в домашней дериктроии
```
$ pwd
/home/username

$mkdir .zshfn

$ ls -la
drwxr-xr-x   2 username username      21 Jan 25 13:33 .zshfn
-rw-------   1 username username  424935 Jan 29 09:48 .zsh_history
-rw-rw-r--.  1 username username    1212 Jan 25 13:33 .zshrc
```
### Добавить следующие строки в файл .zshrc
```
fpath=( ~/.zshfn "${fpath[@]}" )
autoload -Uz $fpath[1]/*(.:t)
```
### Скопировать или создать функцию в дериктории ~/.zshfn
```
$ cd ~/.zshfn && ls
gitpush

$ cat  gitpush

gitpush() {
   git add .
   git commit -m "$(git branch --show-current), $1"
   git push origin $(git branch --show-current)
}
```
### Перечитать содержимое ~/.zshrc (или можно перелогиниться)
```
$ source ~/.zshrc
```

### Теперь можно использовать функцию где угодно (в сеансе текущего пользователя)
```
$ gitpush 'add functions of bash'  

[bash_function 1db4614] bash_function, add functions of bash
    1 file changed, 86 insertions(+)
    create mode 100644 bash_function/README.md
    Enumerating objects: 5, done.
    Counting objects: 100% (5/5), done.
    Delta compression using up to 8 threads
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (4/4), 1.59 KiB | 1.59 MiB/s, done.
    Total 4 (delta 0), reused 0 (delta 0)
    remote: 
    remote: Create pull request for bash_function:
    remote:   ****************************************
    remote: 
    To ssh://**********.git
    * [new branch]      bash_function -> bash_function

```